﻿using Moq;
using NUnit.Framework;
using Payroll.Deductions;
using System.Collections.Generic;

namespace Payroll.Test.Deductions
{
    [TestFixture]
    public class ListDeductionTest
    {
        private Mock<IDeduction> firstDeduction;
        private Mock<IDeduction> secondDeduction;

        private ListDeduction subject;

        [SetUp]
        public void SetUp()
        {
            firstDeduction = new Mock<IDeduction>();
            secondDeduction = new Mock<IDeduction>();
            var deductions = new List<IDeduction>
            {
                firstDeduction.Object,
                secondDeduction.Object
            };

            subject = new ListDeduction(deductions);
        }

        [Test]
        public void Apply()
        {
            double value = 50;

            firstDeduction.Setup(d => d.Apply(ref value)).Returns(5);
            secondDeduction.Setup(d => d.Apply(ref value)).Returns(10);

            var result = subject.Apply(ref value);

            Assert.That(result, Is.EqualTo(15));
        }

        [Test]
        public void Name()
        {
            const string name = nameof(name);

            subject.Name = name;

            var result = subject.Name;

            Assert.That(result, Is.SameAs(name));
        }
    }
}