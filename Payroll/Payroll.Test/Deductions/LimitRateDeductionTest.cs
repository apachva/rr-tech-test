﻿using NUnit.Framework;
using Payroll.Deductions;

namespace Payroll.Test.Deductions
{
    [TestFixture]
    public class LimitRateDeductionTest
    {
        private const double limit = 100;
        private const double tax = 0.05;

        private LimitRateDeduction subject;

        [SetUp]
        public void SetUp()
        {
            subject = new LimitRateDeduction(limit, tax);
        }

        [TestCase(50, 2.5)]
        [TestCase(100, 5)]
        public void Apply_When_Value_Less_Or_Equal_Limit(double value, double expected)
        {
            var result = subject.Apply(ref value);

            Assert.That(result, Is.EqualTo(expected));
            Assert.That(value, Is.EqualTo(0));
        }

        [Test]
        public void Apply_When_Value_More_Than_Limit()
        {
            double value = 150;

            var result = subject.Apply(ref value);

            Assert.That(result, Is.EqualTo(5));
            Assert.That(value, Is.EqualTo(50));
        }

        [Test]
        public void Name()
        {
            const string name = nameof(name);

            subject.Name = name;

            var result = subject.Name;

            Assert.That(result, Is.SameAs(name));
        }
    }
}