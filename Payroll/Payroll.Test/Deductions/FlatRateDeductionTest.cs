﻿using NUnit.Framework;
using Payroll.Deductions;

namespace Payroll.Test.Deductions
{
    [TestFixture]
    public class FlatRateDeductionTest
    {
        private const double tax = 0.05;

        private FlatRateDeduction subject;

        [SetUp]
        public void SetUp()
        {
            subject = new FlatRateDeduction(tax);
        }

        [Test]
        public void Apply()
        {
            double value = 100;

            var result = subject.Apply(ref value);

            Assert.That(result, Is.EqualTo(5));
        }

        [Test]
        public void Name()
        {
            const string name = nameof(name);

            subject.Name = name;

            var result = subject.Name;

            Assert.That(result, Is.SameAs(name));
        }
    }
}