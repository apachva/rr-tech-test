﻿using NUnit.Framework;
using Payroll.Deductions;

namespace Payroll.Test.Deductions
{
    [TestFixture]
    public class StepRateDeductionTest
    {
        private const double stepAmount = 100;
        private const double baseTax = 0.1;
        private const double stepTax = 0.01;

        private StepRateDeduction subject;

        [SetUp]
        public void SetUp()
        {
            subject = new StepRateDeduction(stepAmount, baseTax, stepTax);
        }

        [TestCase(550, 68)]
        [TestCase(30, 3.3)]
        public void Apply(double value, double expected)
        {
            var result = subject.Apply(ref value);

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Name()
        {
            const string name = nameof(name);

            subject.Name = name;

            var result = subject.Name;

            Assert.That(result, Is.SameAs(name));
        }
    }
}