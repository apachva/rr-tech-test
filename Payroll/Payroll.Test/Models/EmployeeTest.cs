﻿using NUnit.Framework;
using Payroll.Exceptions;
using Payroll.Models;
using System;

namespace Payroll.Test.Models
{
    [TestFixture]
    public class EmployeeTest
    {
        [Test]
        public void Constructor()
        {
            const Location location = Location.Germany;
            const int hoursWorked = 160;
            const double hourlyRate = 10.5;

            var result = new Employee(location, hoursWorked, hourlyRate);

            Assert.That(result.Location, Is.EqualTo(location));
            Assert.That(result.HoursWorked, Is.EqualTo(hoursWorked));
            Assert.That(result.HourlyRate, Is.EqualTo(hourlyRate));
        }

        [Test]
        public void Constructor_Returns_AggregateException_When_HoursWorked_Is_Negative()
        {
            const Location location = Location.Germany;
            const int hoursWorked = -10;
            const double hourlyRate = 10.5;

            var result = Assert.Throws<AggregateException>(() => { new Employee(location, hoursWorked, hourlyRate); });
            var innerExceptionsResult = result.InnerExceptions;

            Assert.That(innerExceptionsResult.Count, Is.EqualTo(1));
            Assert.That(innerExceptionsResult[0], Is.TypeOf<NegativeValueException>());
            Assert.That(innerExceptionsResult[0].Message, Is.EqualTo("HoursWorked cannot be negative.\r\nParameter name: HoursWorked"));
        }

        [Test]
        public void Constructor_Returns_AggregateException_When_HourlyRate_Is_Negative()
        {
            const Location location = Location.Germany;
            const int hoursWorked = 10;
            const double hourlyRate = -10.5;

            var result = Assert.Throws<AggregateException>(() => { new Employee(location, hoursWorked, hourlyRate); });
            var innerExceptionsResult = result.InnerExceptions;

            Assert.That(innerExceptionsResult.Count, Is.EqualTo(1));
            Assert.That(innerExceptionsResult[0], Is.TypeOf<NegativeValueException>());
            Assert.That(innerExceptionsResult[0].Message, Is.EqualTo("HourlyRate cannot be negative.\r\nParameter name: HourlyRate"));
        }
    }
}