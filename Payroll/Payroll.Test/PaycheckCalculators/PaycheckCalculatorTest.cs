﻿using Moq;
using NUnit.Framework;
using Payroll.Deductions;
using Payroll.Models;
using Payroll.PaycheckCalculators;
using System.Collections.Generic;

namespace Payroll.Test.PaycheckCalculators
{
    [TestFixture]
    public class PaycheckCalculatorTest
    {
        private Mock<IDeduction> firstDeduction;
        private Mock<IDeduction> secondDeduction;

        private PaycheckCalculator subject;

        [SetUp]
        public void SetUp()
        {
            firstDeduction = new Mock<IDeduction>();
            secondDeduction = new Mock<IDeduction>();
            var deductions = new List<IDeduction>
            {
                firstDeduction.Object,
                secondDeduction.Object
            };

            subject = new PaycheckCalculator(deductions);
        }

        [Test]
        public void CalculatePayCheck()
        {
            const Location location = Location.Germany;
            const int hoursWorked = 160;
            const double hourlyRate = 10.5;
            const string firstDeductionName = nameof(firstDeductionName);
            const double firstDeductionValue = 40;
            const string secondDeductionName = nameof(secondDeductionName);
            const double secondDeductionValue = 50;
            var expectedGrossSalary = hoursWorked * hourlyRate;
            var employee = new Employee(location, hoursWorked, hourlyRate);

            firstDeduction.Setup(d => d.Name).Returns(firstDeductionName);
            firstDeduction.Setup(d => d.Apply(ref expectedGrossSalary)).Returns(firstDeductionValue);
            secondDeduction.Setup(d => d.Name).Returns(secondDeductionName);
            secondDeduction.Setup(d => d.Apply(ref expectedGrossSalary)).Returns(secondDeductionValue);

            var result = subject.CalculatePayCheck(employee);
            var deductionsResult = result.Deductions;

            Assert.That(result.Location, Is.EqualTo(location));
            Assert.That(result.GrossSalary, Is.EqualTo(expectedGrossSalary));
            Assert.That(deductionsResult.Count, Is.EqualTo(2));
            Assert.That(deductionsResult.ContainsKey(firstDeductionName), Is.True);
            Assert.That(deductionsResult[firstDeductionName], Is.EqualTo(firstDeductionValue));
            Assert.That(deductionsResult.ContainsKey(secondDeductionName), Is.True);
            Assert.That(deductionsResult[secondDeductionName], Is.EqualTo(secondDeductionValue));
        }
    }
}
