﻿using NUnit.Framework;
using Payroll.Models;
using Payroll.ViewModels;
using System.Collections.Generic;
using System.Text;

namespace Payroll.Test.ViewModels
{
    [TestFixture]
    public class PaycheckTest
    {
        [Test]
        public void ToString()
        {
            const Location location = Location.Germany;
            const double grossSalary = 100;
            const double netSalary = 50;
            const string deductionName = "Pension";
            const double deductionValue = 50;
            var subject = new Paycheck
            {
                Location = location,
                GrossSalary = grossSalary,
                NetSalary = netSalary,
                Deductions = new Dictionary<string, double> { { deductionName, deductionValue } }
            };
            var builder = new StringBuilder();
            builder.Append($"Employee location: {location}\n\n");
            builder.Append($"Gross Amount: €{grossSalary}\n\n");
            builder.Append($"Less deductions\n\n");
            builder.Append($"{deductionName}: €{deductionValue}\n");
            builder.Append("\n");
            builder.Append($"Net Amount: €{netSalary}");

            var result = subject.ToString();

            Assert.That(result, Is.EqualTo(builder.ToString()));
        }
    }
}