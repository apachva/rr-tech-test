﻿using NUnit.Framework;
using Payroll.Exceptions;
using Payroll.Mappers;
using Payroll.Models;

namespace Payroll.Test.Mappers
{
    [TestFixture]
    public class LocationMapperTest
    {
        private LocationMapper subject;

        [SetUp]
        public void SetUp()
        {
            subject = new LocationMapper();
        }

        [TestCase("Ireland", Location.Ireland)]
        [TestCase("Italy", Location.Italy)]
        [TestCase("Germany", Location.Germany)]
        [TestCase("germany", Location.Germany)]
        public void Map(string locationStr, Location expected)
        {
            var result = subject.Map(locationStr);

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Map_Returns_InvalidLocationException_When_Location_Is_Invalid()
        {
            const string location = "location";

            Assert.Throws<InvalidLocationException>(() => { subject.Map(location); });
        }
    }
}