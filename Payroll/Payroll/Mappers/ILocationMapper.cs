﻿using Payroll.Models;

namespace Payroll.Mappers
{
    public interface ILocationMapper
    {
        Location Map(string locationStr);
    }
}