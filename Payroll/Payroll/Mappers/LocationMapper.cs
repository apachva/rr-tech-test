﻿using System;
using Payroll.Models;
using Payroll.Exceptions;

namespace Payroll.Mappers
{
    public class LocationMapper : ILocationMapper
    {
        public Location Map(string locationStr)
        {
            Location location;

            if (!Enum.TryParse(locationStr, true, out location))
            {
                throw new InvalidLocationException();
            }

            return location;
        }
    }
}