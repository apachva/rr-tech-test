﻿using Payroll.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Payroll.ViewModels
{
    public class Paycheck
    {
        public double GrossSalary { get; set; }
        public double NetSalary { get; set; }
        public Location Location { get; set; }
        public IDictionary<string, double> Deductions { get; set; } = new Dictionary<string, double>();

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append($"Employee location: {Location}\n\n");
            builder.Append($"Gross Amount: €{GrossSalary}\n\n");
            builder.Append($"Less deductions\n\n");

            foreach (var deduction in Deductions)
            {
                builder.Append($"{deduction.Key}: €{deduction.Value}\n");
            }

            builder.Append("\n");
            builder.Append($"Net Amount: €{NetSalary}");

            return builder.ToString();
        }
    }
}