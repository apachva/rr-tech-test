﻿using System;

namespace Payroll.Exceptions
{
    public class NegativeValueException : ArgumentException
    {
        public NegativeValueException() : base("Value cannot be less than zero.") { }

        public NegativeValueException(string message) : base(message) { }
        public NegativeValueException(string message, string paramName) : base(message, paramName) { }
    }
}