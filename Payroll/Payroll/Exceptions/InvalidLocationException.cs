﻿using System;

namespace Payroll.Exceptions
{
    public class InvalidLocationException : Exception
    {
        public InvalidLocationException() : base("Invalid location.") { }

        public InvalidLocationException(string message) : base(message) { }

        public InvalidLocationException(string message, Exception inner) : base(message, inner) { }
    }
}