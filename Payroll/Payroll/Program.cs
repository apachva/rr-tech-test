﻿using Payroll.Mappers;
using Payroll.Models;
using Payroll.PaycheckCalculators;
using System;
using System.Linq;

namespace Payroll
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            try
            {
                Console.Write("Please enter the hours worked: ");
                var hoursWorked = int.Parse(Console.ReadLine());

                Console.Write("Please enter the hourly rate: ");
                var hourlyRate = double.Parse(Console.ReadLine());

                Console.Write("Please enter the employee’s location: ");
                var locationStr = Console.ReadLine();
                var locationMapper = new LocationMapper();
                var location = locationMapper.Map(locationStr);

                var employee = new Employee(location, hoursWorked, hourlyRate);

                var paycheckCalculatorFactory = new PaycheckCalculatorFactory();
                var calculator = paycheckCalculatorFactory.GetPaycheckCalculator(employee);
                if (calculator != null)
                {
                    var paycheck = calculator.CalculatePayCheck(employee);
                    Console.WriteLine(paycheck.ToString());
                }
                else
                {
                    Console.WriteLine($"Paycheck calculator isn't supported for {location}");
                }
            }
            catch (AggregateException ex)
            {
                var errorMessage = string.Join("\n", ex.InnerExceptions.Select(s => s.Message));
                Console.WriteLine(errorMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}