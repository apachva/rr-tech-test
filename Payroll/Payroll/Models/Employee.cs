﻿using Payroll.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Payroll.Models
{
    public class Employee
    {
        public Location Location { get; private set; }

        public int HoursWorked { get; private set; }

        public double HourlyRate { get; private set; }

        public Employee(Location location, int hoursWorked = 0, double hourlyRate = 0.0)
        {
            Location = location;
            HoursWorked = hoursWorked;
            HourlyRate = hourlyRate;

            var errors = Validate();
            if (errors.Count() > 0)
            {
                var aggregateException = new AggregateException(errors);
                throw aggregateException;
            }
        }

        private IEnumerable<ArgumentException> Validate()
        {
            if (HoursWorked < 0)
                yield return new NegativeValueException($"{nameof(HoursWorked)} cannot be negative.", nameof(HoursWorked));

            if (HourlyRate < 0)
                yield return new NegativeValueException($"{nameof(HourlyRate)} cannot be negative.", nameof(HourlyRate));
        }
    }
}