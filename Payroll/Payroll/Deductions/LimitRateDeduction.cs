﻿using System;

namespace Payroll.Deductions
{
    public class LimitRateDeduction : IDeduction
    {
        private readonly double limit;
        private readonly double tax;

        public LimitRateDeduction(double limit, double tax)
        {
            this.limit = limit;
            this.tax = tax;
        }

        public string Name { get; set; }

        public double Apply(ref double value)
        {
            double deduction = 0;
            if (value <= limit)
            {
                deduction = value * tax;
                value = 0;
            }
            else
            {
                deduction = limit * tax;
                value = value - limit;
            }

            return deduction;
        }
    }
}