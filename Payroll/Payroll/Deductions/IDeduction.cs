﻿namespace Payroll.Deductions
{
    public interface IDeduction
    {
        string Name { get; set; }
        double Apply(ref double value);
    }
}