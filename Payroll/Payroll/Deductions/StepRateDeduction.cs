﻿namespace Payroll.Deductions
{
    public class StepRateDeduction : IDeduction
    {
        private readonly double stepAmount;
        private readonly double baseTax;
        private readonly double stepTax;

        public StepRateDeduction(double stepAmount, double baseTax, double stepTax)
        {
            this.stepAmount = stepAmount;
            this.baseTax = baseTax;
            this.stepTax = stepTax;
        }

        public string Name { get; set; }

        public double Apply(ref double value)
        {
            int numberOfSteps = (int)(value / stepAmount);
            double tax = 0;
            for (int i = 0; i < numberOfSteps; i++)
            {
                tax += baseTax + i * stepTax;
            }
            double taxSuma = stepAmount * tax;
            taxSuma += (value % stepAmount) * ((numberOfSteps + 1) * stepTax + baseTax);

            return taxSuma;
        }
    }
}