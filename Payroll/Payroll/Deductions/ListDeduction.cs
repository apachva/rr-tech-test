﻿using System;
using System.Collections.Generic;

namespace Payroll.Deductions
{
    public class ListDeduction : IDeduction
    {
        private readonly IEnumerable<IDeduction> deductions;

        public ListDeduction(IEnumerable<IDeduction> deductions)
        {
            this.deductions = deductions;
        }

        public string Name { get; set; }

        public double Apply(ref double value)
        {
            double total = 0;

            foreach (var deduction in deductions)
            {
                total += deduction.Apply(ref value);
            }

            return total;
        }
    }
}