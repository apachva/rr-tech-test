﻿using System;

namespace Payroll.Deductions
{
    public class FlatRateDeduction : IDeduction
    {
        private readonly double tax;

        public FlatRateDeduction(double tax)
        {
            this.tax = tax;
        }

        public string Name { get; set; }

        public double Apply(ref double value)
        {
            return value * tax;
        }
    }
}