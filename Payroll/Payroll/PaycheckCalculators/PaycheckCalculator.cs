﻿using Payroll.Deductions;
using System.Collections.Generic;
using Payroll.Models;
using Payroll.ViewModels;

namespace Payroll.PaycheckCalculators
{
    public class PaycheckCalculator : IPaycheckCalculator
    {
        private readonly IEnumerable<IDeduction> deductions;

        public PaycheckCalculator(IEnumerable<IDeduction> deductions)
        {
            this.deductions = deductions;
        }

        public Paycheck CalculatePayCheck(Employee employee)
        {
            var payCheck = new Paycheck
            {
                GrossSalary = employee.HourlyRate * employee.HoursWorked,
                Location = employee.Location
            };

            CalculatePaycheck(payCheck);

            return payCheck;
        }

        protected void CalculatePaycheck(Paycheck paycheck)
        {
            double totalDeductions = 0;
            foreach (var deduction in deductions)
            {
                double grossSalary = paycheck.GrossSalary;
                var deductionValue = deduction.Apply(ref grossSalary);
                totalDeductions += deductionValue;

                paycheck.Deductions.Add(deduction.Name, deductionValue);
            }

            paycheck.NetSalary = paycheck.GrossSalary - totalDeductions;
        }
    }
}