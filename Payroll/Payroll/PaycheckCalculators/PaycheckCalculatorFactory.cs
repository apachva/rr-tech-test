﻿using Payroll.Deductions;
using Payroll.Models;
using System.Collections.Generic;

namespace Payroll.PaycheckCalculators
{
    public class PaycheckCalculatorFactory : IPaycheckCalculatorFactory
    {
        public IPaycheckCalculator GetPaycheckCalculator(Employee employee)
        {
            switch (employee.Location)
            {
                case Location.Ireland:
                    return GetIreland();
                case Location.Italy:
                    return GetItaly();
                case Location.Germany:
                    return GetGermany();
                default:
                    return null;
            }
        }

        private static IPaycheckCalculator GetIreland()
        {
            var incomeDeductions = new List<IDeduction>
            {
                new LimitRateDeduction(600, 0.25),
                new FlatRateDeduction(0.4)
            };
            var socialDeductions = new List<IDeduction>
            {
                new LimitRateDeduction(500, 0.07),
                new FlatRateDeduction(0.08)
            };

            var deductions = new List<IDeduction>
            {
                new ListDeduction(incomeDeductions) { Name = "Income Tax" },
                new ListDeduction(socialDeductions) { Name = "Universal Social Charge" },
                new FlatRateDeduction(0.04) { Name = "Pension" }
            };

            return new PaycheckCalculator(deductions);
        }

        private static IPaycheckCalculator GetItaly()
        {
            var inpsDeductions = new List<IDeduction>
            {
                new LimitRateDeduction(500, 0.09),
                new StepRateDeduction(100, 0.10, 0.01)
            };

            var deductions = new List<IDeduction>
            {
                new FlatRateDeduction(0.25) { Name = "Income Tax" },
                new ListDeduction(inpsDeductions) { Name= "INPS"}
            };

            return new PaycheckCalculator(deductions);
        }

        private static IPaycheckCalculator GetGermany()
        {
            var incomeDeductions = new List<IDeduction>
            {
                new LimitRateDeduction(400, 0.25),
                new FlatRateDeduction(0.32)
            };

            var deductions = new List<IDeduction>
            {
                new ListDeduction(incomeDeductions) { Name ="Income Tax"},
                new FlatRateDeduction(0.02) { Name = "Pension" }
            };

            return new PaycheckCalculator(deductions);
        }
    }
}