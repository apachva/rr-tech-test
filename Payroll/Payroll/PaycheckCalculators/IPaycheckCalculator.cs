﻿using Payroll.Models;
using Payroll.ViewModels;

namespace Payroll.PaycheckCalculators
{
    public interface IPaycheckCalculator
    {
        Paycheck CalculatePayCheck(Employee employee);
    }
}