﻿using Payroll.Models;

namespace Payroll.PaycheckCalculators
{
    public interface IPaycheckCalculatorFactory
    {
        IPaycheckCalculator GetPaycheckCalculator(Employee employee);
    }
}